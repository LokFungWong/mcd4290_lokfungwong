/*question 5*/
/*The problem of this expression is that when the year changes to 2015 or 2016, the output is not false but remains true.*/
/* Solution: Change || to && */

let year;
let yearNot2015Or2016;
year = 2000;
yearNot2015Or2016 = year!= 2015 && year != 2016;
console.log(yearNot2015Or2016);