//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output;// = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    output = "question 1 output" + "\n";
    let data = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let pOdd = [];
    let nEven = [];
    let i = 0;
    for (i = 0; i<100; i++){
        if (data[i] % 2 == 1 && data[i] > 0){
            pOdd.push(data[i]);
        }
        if (data[i] % 2 == 0 && data[i] < 0){
            nEven.push(data[i]);
        }
    }
    output += "Postive Odd: " + pOdd + "\n" + "Negative Even: " + nEven;
    console.log(output);
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output;// = "" 
    
    //Question 2 here
    output = "question 2 output" + "\n";
    let i = 0;
    let frequency1 = 0;
    let frequency2 = 0;
    let frequency3 = 0;
    let frequency4 = 0;
    let frequency5 = 0;
    let frequency6 = 0;
    for(i=0;i<60000;i++){
    let die = Math.floor((Math.random() * 6) + 1);
        if(die===1){
            frequency1 +=1;
        }
        else if(die===2){
            frequency2 +=1;
        }
        else if(die===3){
            frequency3 +=1;
        }
        else if(die===4){
            frequency4 +=1;
        }
        else if(die===5){
            frequency5 +=1;
        }
        else if(die===6){
            frequency6 +=1;
        }
    }
    output += "Frequency of die rolls" + "\n";
    output += "1: " + frequency1 + "\n";
    output += "2: " + frequency2 + "\n";
    output += "3: " + frequency3 + "\n";
    output += "4: " + frequency4 + "\n";
    output += "5: " + frequency5 + "\n";
    output += "6: " + frequency6 + "\n";
    console.log(output);
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output;// = "" 
    
    //Question 3 here
    output = "question 3 output" + "\n";
    let i = 0;
    let frequency = [0, 0, 0, 0, 0, 0, 0];
    for(i=0;i<60000;i++){
        let die = Math.floor((Math.random() * 6) + 1);
        frequency[die] += 1;
    }    
    output += "Frequency of die rolls" + "\n";
    let j = 1;
    for (j=1;j<=6;j++){
    output += j + ":" + frequency[j] +"\n";
    }
    console.log(output); 
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output;// = "" 
    
    //Question 4 here
    output = "question 4 output" + "\n";
    let i = 0;
    var dieRolls = {
            Frequencies: {
                  1: 0,
                  2: 0,
                  3: 0,
                  4: 0,
                  5: 0,
                  6: 0,
            },
            Total:60000,
            Exceptions:""
    }
    for(i=0;i<60000;i++){
        let die = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[die] += 1;
    }    
    output += "Frequency of die rolls" + "\n";
    output += "Total rolls: " + dieRolls.Total + "\n";
    let x = 0;
    for (x in dieRolls.Frequencies){
        output += x + ": " + dieRolls.Frequencies[x] +"\n";
        if (dieRolls.Frequencies[x] / 10000 > 1.01 || dieRolls.Frequencies[x] / 10000 < 0.99){
          dieRolls.Exceptions += x + " ";
        }
    }
    output += "Exceptions: " + dieRolls.Exceptions;
    console.log(output);

    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output;// = "" 
    
    //Question 5 here
    output = "question 5 output" + "\n";
    let person = {
        name: "Jane",
        income: 127050
    }
    let tax = 0;
    output += person.name + "’s income is: $" + person.income + " ";
    output += "and her tax owed is: $"
    if (person.income > 18200 && person.income <= 37000){
        tax = (person.income - 18200) * 0.19;
    }
    else if (person.income > 37000 && person.income <= 90000){
        tax = 3572 + (person.income - 37000) * 0.325;
    }
    else if (person.income > 90000 && person.income <= 180000){
        tax = 20797 + (person.income - 90000) * 0.37;
    }
    else if (person.income > 180000){
        tax = 54097 + (person.income - 180000) * 0.45;
    }
      output += tax.toFixed(2);
    console.log(output);
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}